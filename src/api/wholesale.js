// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from "@/utils/request";

/**
 * 新增批发商品
 * @param pram
 */
export function productBiddingSaveApi(data) {
  return request({
    url: "/admin/store/biddingProduct/biddingSave",
    method: "POST",
    data,
  });
}

/**
 * 批发商品列表
 * @param pram
 */
export function productGetbiddingListApi(params) {
  return request({
    url: "/admin/store/biddingProduct/getbiddingList",
    method: "GET",
    params,
  });
}

/**
 * 修改商品
 * @param pram
 */
export function productBiddingUpdateApi(data) {
  return request({
    url: "/admin/store/biddingProduct/biddingUpdate",
    method: "POST",
    data,
  });
}

/**
 * 商品删除
 * @param pram
 */
export function productBiddingBiddingDelete(id, type) {
  return request({
    url: `/admin/store/biddingProduct/biddingDelete/${id}`,
    method: "GET",
    params: { type: type },
  });
}

/**
 * 商品下架
 * @param pram
 */
export function productBiddingSellBiddingProduct(id) {
  return request({
    url: `/admin/store/biddingProduct/sellBiddingProduct/${id}`,
    method: "GET",
  });
}
